import React, { Component } from 'react';
import coverImage from './resource/socialMedia.jpg';
class Cover extends Component {
  render() {
    return (
      <div class="home">
      <div className = 'coverImage'>
        <img style={{width: "auto", height: "auto"}} src={coverImage} alt ='cover' height="550" width ="700"/>
      </div>
        <div class="col-xs-12">
          <div class="hero-text">
            <h1>
              Transmodus
              Follower Growth
            </h1>
            <p class ="first">
              Our service helps you grow your follower in very short time through marketing techniques
            </p>
            <p class="second">
              short time, transmodus growth
            </p>
          </div>          
        </div>
      </div>
    );
  }
}

export default Cover;
