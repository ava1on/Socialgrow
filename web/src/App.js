import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from "./components/navBar.js";
import Cover from "./components/cover.js";

class App extends Component {
  render() {
    return (
        <div>
            <NavBar />
            <Cover />
        </div>
    );
  }
}

export default App;
